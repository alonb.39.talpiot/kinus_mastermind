import pyeasyga
import random

def str2clue(str):
    exs = 0
    os = 0
    for element in str:
        if element == "X":
            exs += 1
        if element == "O":
            os += 1
    return exs, os

def get_relative_clue(guess, code):
    temp_clues = []
    temp = code.copy()  # need to copy the code array, so we can change it

    holdguess = list(guess)

    # loop to check for black pegs
    for i in range(len(code)):
        if holdguess[i] == code[i]:
            temp_clues.append("X")  # black peg-correct letter and position
            temp[i] = "M"  # remove correct location from code checking array
            holdguess[i] = "Q"  # take out of guess so not counted as white peg

    # loop to check for white pegs
    for j in range(len(code)):
        if temp[j] in holdguess:
            temp_clues.append("O")  # white peg-correct letter, wrong position
            holdguess.remove(temp[j])  # remove from check so not counted twice

    temp_clues.sort(reverse=True)  # sort so X's are always first so player can't tell which are correct
    clue = "".join(temp_clues)
    return clue

class GAWRAPPER:
    def __init__(self, colors, num_pegs, playerguesses, clues):

        self.data = [colors, num_pegs, playerguesses, clues]
        '''
        (data, population_size, generations, crossover_probability, mutation_probability, elitism=True, maximise_fitness=True)
        '''
        self.ga = pyeasyga.GeneticAlgorithm(self.data, 150, 20, 0.5, 0.06, True, True)

        def create_individual(data):
            letters = data[0]
            result_str = []
            for i in range(data[1]):
                result_str.append(random.choice(letters))
            return result_str

        self.ga.create_individual = create_individual

        def crossover(parent_1, parent_2):
            index = random.randrange(1, len(parent_1))
            child_1 = parent_1[:index] + parent_2[index:]
            child_2 = parent_2[:index] + parent_1[index:]
            return child_1, child_2

        self.ga.crossover_function = crossover

        def mutate(individual):
            mutvsswitchchance = 0.5
            temp = random.uniform(0, 1)

            if temp < mutvsswitchchance:
                color_index = random.randrange(len(self.data[0]))
                mutate_index = random.randrange(len(individual))
                individual[mutate_index] = self.data[0][color_index]
            else:
                mutate_index1 = random.randrange(len(individual))
                mutate_index2 = random.randrange(len(individual))
                temp = individual[mutate_index1]
                individual[mutate_index1] = individual[mutate_index2]
                individual[mutate_index2] = temp

        self.ga.mutate_function = mutate

        def selection(population):
            return random.choice(population)

        self.ga.selection_function = selection

        def fitness(individual, data):
            a = 5
            res = 0
            playerguesses = data[2]
            clues = data[3]
            for i in range(len(playerguesses)):
                rclue = str2clue(get_relative_clue(individual, playerguesses[i]))
                clue = str2clue(clues[i])

                res -= a*abs(clue[0] - rclue[0]) + abs(clue[1] - rclue[1])
            return res

        self.ga.fitness_function = fitness

    def run_ga(self):
        self.ga.run()
        return "".join(self.ga.best_individual()[1])

