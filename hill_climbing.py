# succ. of a state is the same state with a single color switched
# state is represented by ABCD
import random
def str2clue(str):
    exs = 0
    os = 0
    for element in str:
        if element == 'X':
            exs += 1
        if element == 'O':
            os += 1
    return exs, os

def get_relative_clue(guess, code):
    temp_clues = []
    temp = code.copy()  # need to copy the code array, so we can change it

    holdguess = list(guess)

    # loop to check for black pegs
    for i in range(len(code)):
        if holdguess[i] == code[i]:
            temp_clues.append("X")  # black peg-correct letter and position
            temp[i] = "M"  # remove correct location from code checking array
            holdguess[i] = "Q"  # take out of guess so not counted as white peg

    # loop to check for white pegs
    for j in range(len(code)):
        if temp[j] in holdguess:
            temp_clues.append("O")  # white peg-correct letter, wrong position
            holdguess.remove(temp[j])  # remove from check so not counted twice

    temp_clues.sort(reverse=True)  # sort so X's are always first so player can't tell which are correct
    clue = "".join(temp_clues)
    return clue


def choose_guess(colors, num_pegs, guesses, clues):
    if not guesses: #nothing was guessed yet
        c = ''
        for i in range(num_pegs):
            a = random.choice(colors)
            c += a
        return c
    current = guesses[-1]
    while True:
        cur_succ = get_succ(current, colors)
        max_succ = current
        max_value = fitness(current, guesses, clues)
        for succ in cur_succ:
            succ_fitness = fitness(succ, guesses, clues)
            if succ_fitness >= max_value:
                max_succ = succ
                max_value = succ_fitness
        if max_value == fitness(current, guesses, clues):
            return ''.join(current)
        current = max_succ




def get_succ(guess, colors):
    succ = []
    for i in range(len(guess)):
        for j in colors:
            if guess[i] != j:
                succ.append(guess[:i] + [j] + guess[i+1:])
    return succ

def fitness(individual, guesses, clues):
    a = 5
    res = 0
    for i in range(len(guesses)):
        rclue = str2clue(get_relative_clue(individual, guesses[i]))
        clue = str2clue(clues[i])
        res -= a*abs(clue[0] - rclue[0]) + abs(clue[1] - rclue[1])
    return res
