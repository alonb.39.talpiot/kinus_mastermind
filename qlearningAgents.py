# qlearningAgents.py
# ------------------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html


from learningAgents import ReinforcementAgent
import random,util,math
import abc
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np


class QLearningAgent(ReinforcementAgent):
  """
    Q-Learning Agent

    Instance variables you have access to
      - self.epsilon (exploration prob)
      - self.alpha (learning rate)
      - self.discount (discount rate)

    Functions you should use
      - self.getLegalActions(state)
        which returns legal actions
        for a state
  """
  def __init__(self, **args):
    "You can initialize Q-values here..."
    ReinforcementAgent.__init__(self, **args)
    self.values = util.Counter()

  def getQValue(self, state, action):
    """
      Returns Q(state,action)
      Should return 0.0 if we never seen
      a state or (state,action) tuple
    """
    # values is a util.Counter and return 0 for an unseen tuple of (state,action)
    return self.values[(state,action)]


  def max_action(self,state):
    """return the max value and max action for a state
    as a tuple (max_action,max_value)"""
    leagal_actions = self.getLegalActions(state)
    if leagal_actions is None:
      return None,0  # terminal state
    if len(leagal_actions) == 0:
      return None,0  # terminal state
    max_action = None
    max_value = float('-inf')
    for action in leagal_actions:
      value = self.getQValue(state, action)
      if value > max_value:
        max_value = value
        max_action = action
    return max_action,max_value

  def getValue(self, state):
    """
      Returns max_action Q(state,action)
      where the max is over legal actions.  Note that if
      there are no legal actions, which is the case at the
      terminal state, you should return a value of 0.0.
    """
    return self.max_action(state)[1]

  def getPolicy(self, state):
    """
      Compute the best action to take in a state.  Note that if there
      are no legal actions, which is the case at the terminal state,
      you should return None.
    """
    return self.max_action(state)[0]

  def getAction(self, state):
    """
      Compute the action to take in the current state.  With
      probability self.epsilon, we should take a random action and
      take the best policy action otherwise.  Note that if there are
      no legal actions, which is the case at the terminal state, you
      should choose None as the action.

      HINT: You might want to use util.flipCoin(prob)
      HINT: To pick randomly from a list, use random.choice(list)
    """
    # Pick Action
    leagal_actions = self.getLegalActions(state)
    action = None
    if leagal_actions is None:
      return action  # terminal state
    if len(leagal_actions) == 0:
      return action  # terminal state
    p = random.random()
    if p < self.epsilon:
      return random.choice(leagal_actions)
    else:
      return self.getPolicy(state)


  def update(self, state, action, nextState, reward):
    """
      The parent class calls this to observe a
      state = action => nextState and reward transition.
      You should do your Q-Value update here

      NOTE: You should never call this function,
      it will be called on your behalf
    """
    self.values[(state,action)] += self.alpha*(reward+self.discount*self.getValue(nextState)
                                               -self.values[(state,action)])




class MMNet(nn.Module):
  def __init__(self,state_size):
    super(MMNet,self).__init__()
    # num of states is a vector
    self.state_size = state_size
    # self.LFirst = nn.Linear(state_size,100)
    # self.L1 = nn.Linear(100,100)
    # self.L2 = nn.Linear(100, 100)
    # self.L3 = nn.Linear(100, 100)
    # self.LFinal = nn.Linear(100,state_size)
    self.full1 = nn.Linear(state_size,state_size)
    # self.full2 = nn.Linear(state_size, state_size)

  def forward(self,x):
    x = torch.tanh(self.full1(x))
    # x = torch.tanh(self.full2(x))
    # x = torch.tanh(self.LFirst(x))
    # x = torch.tanh(self.L1(x))
    # x = torch.tanh(self.L2(x))
    # x = self.LFinal(x)
    return x

class MMDeepQLearning(ReinforcementAgent):
  def __init__(self,game, **args):
    ReinforcementAgent.__init__(self, **args)
    self.game = game
    self.q_net = MMNet(game.code_options_num) # code_options_num = colors**pegs
    self.target_net = MMNet(game.code_options_num)
    self.train = True
    self.optimizer = optim.SGD(self.q_net.parameters(), lr=self.alpha)


  def getQValue(self, state, action):
    """
      Returns Q(state,action)
    """
    # values is a util.Counter and return 0 for an unseen tuple of (state,action)
    q_action_vector = self.q_net(state)
    action_index = action.index
    return q_action_vector[action_index]


  def max_action(self,state):
    """return the max value and max action for a state
    as a tuple (max_action,max_value)"""
    leagal_actions = self.game.code_options
    if leagal_actions is None:
      return None,0  # terminal state
    if len(leagal_actions) == 0:
      return None,0  # terminal state
    state = torch.FloatTensor(state)
    with torch.no_grad():
      q_values = self.target_net(state)
      not_valid_idx = (state==0).nonzero(as_tuple=True)[0]
      q_values[not_valid_idx] = float('-inf')
      # for i in range(len(q_values)):
      #   if state[i] == 0: # not a good action
      #     q_values[i] = float('-inf')
    max_value = torch.max(q_values)
    action_index = np.argmax(q_values)
    for action in leagal_actions:
      if self.game.code_to_index(action) == action_index:
        return action,max_value

  def getValue(self, state):
    """
      Returns max_action Q(state,action)
      where the max is over legal actions.  Note that if
      there are no legal actions, which is the case at the
      terminal state, you should return a value of 0.0.
    """
    return self.max_action(state)[1]

  def getPolicy(self, state):
    """
      Compute the best action to take in a state.  Note that if there
      are no legal actions, which is the case at the terminal state,
      you should return None.
    """
    return self.max_action(state)[0]

  def getAction(self, state):
    """
      Compute the action to take in the current state.  With
      probability self.epsilon, we should take a random action and
      take the best policy action otherwise.  Note that if there are
      no legal actions, which is the case at the terminal state, you
      should choose None as the action.

      HINT: You might want to use util.flipCoin(prob)
      HINT: To pick randomly from a list, use random.choice(list)
    """
    # Pick Action
    leagal_actions = self.game.code_options
    action = None
    if leagal_actions is None:
      return action  # terminal state
    if len(leagal_actions) == 0:
      return action  # terminal state
    p = random.random()
    if p < self.epsilon:
      indexes = np.nonzero(state)[0]
      action_index = random.choice(indexes)
      for action in leagal_actions:
        if self.game.code_to_index(action) == action_index:
          return action
    else:
      return self.getPolicy(state)


  def update(self, state, action, nextState, reward):
    """
      The parent class calls this to observe a
      state = action => nextState and reward transition.
      You should do your Q-Value update here

      NOTE: You should never call this function,
      it will be called on your behalf
    """
    self.q_net.zero_grad()
    self.target_net.zero_grad()

    state = torch.FloatTensor(state)
    nextState = torch.FloatTensor(nextState)

    q_state = self.q_net(state)
    action_index = self.game.code_to_index(action)
    q_s_a = q_state[action_index]
    with torch.no_grad():
      q_ns_a = self.getValue(nextState)

    criterion = nn.MSELoss()
    loss = criterion(q_s_a,reward+self.discount*q_ns_a)

    loss.backward()

    self.optimizer.step()
    return loss

  def update_target_net(self):
    self.target_net.load_state_dict(self.q_net.state_dict())

  def save_agent(self,path):
      torch.save(self.target_net.state_dict(),path)

  def load_agent(self,path):
      self.q_net = MMNet(self.game.code_options_num)
      self.target_net = MMNet(self.game.code_options_num)
      self.q_net.load_state_dict(torch.load(path))
      # copy q_net whiets to target net
      self.target_net.load_state_dict(self.q_net.state_dict())

class SimpleMM():
  def __init__(self,game):
    self.game = game

  def getAction(self, state):
    leagal_actions = self.game.code_options
    good_actions = []
    for i in range(len(leagal_actions)):
      if state[i] == 1:
        good_actions.append(leagal_actions[i])
    good_actions = np.array(good_actions)
    action = good_actions[np.random.choice(len(good_actions),1)]
    # print('action:', action)
    return action[0]

  def update(self,state,guess,next_state,reward):
    return 0
