import tkinter as tk
import random
import genetic_algorithem
import hill_climbing
import CSP_player
import qlearningAgents


print("Welcome to MASTERMIND!!!")

is_human = (input("Do you want an AI to play?(y/n):") != "y")
algorithem = None
if not is_human:
    algorithem = input("What kind of player would you like?(genetic/ hill/ csp/ q-learning):")

if algorithem == "q-learning":
    color_number = 6
    codeLength = 4
else:
    color_number = int(input("How many COLORS would you like?:"))
    codeLength = int(input("How many PEGS would you like?:"))

noOfGuesses = int(input("How many TRYS would you like?:"))

num_pegs = codeLength

if is_human:
    print("___________________________________________________")
    print("Instructions:\n",
          "Up/Down Arrows - move between colors\n",
          "Right/Left Arrows - move between pegs\n",
          "Enter - confirm a guess")
    print("___________________________________________________")
else:
    print("___________________________________________________")
    print("Instructions:\n",
          "Enter - ask AI to make a guess")
    print("___________________________________________________")

root = tk.Tk()
frame = tk.Frame(root)

width2pegs = 100
height2turns = 50
canvas = tk.Canvas(frame, width=width2pegs*codeLength,height=height2turns*noOfGuesses, highlightthickness=0,highlightbackground="black", relief=tk.FLAT,bg='#aaaaff',bd=0)

basecolorsdict = ['white','green','red','maroon1','gold','dark orange','dodger blue','grey60']
basecolors = basecolorsdict[:color_number]

position = 0
speed = 2

colorSize = 40
colorpadding = 50

row = 0
cpos = 0

selectColors = []

colorpicks = [[-1 for i in range(codeLength)] for j in range(noOfGuesses)]
clues = [[-1 for i in range(codeLength)] for j in range(noOfGuesses)]

if algorithem == "csp":
    colors = [chr(65 + _) for _ in range(color_number)]
    num_pegs = codeLength
    computer_player = CSP_player.CSPPlayer(colors, num_pegs)

class palti:
    def __init__(self, colors_num, pegs_num):
        self.colors_num = colors_num
        self.colors = [chr(ord("A")+i) for i in range(colors_num)]
        self.pegs_num = pegs_num
        self.code_options_num = colors_num**pegs_num
        self.code_options = self.all_code_options(self.pegs_num)

    def all_code_options(self,pegs):
        all_codes = []
        code = []
        if pegs == 0:
            return [[]]
        for color in self.colors:  # run over all colors
            all_codes_short = self.all_code_options(pegs-1)  # get all codes ends with that color
            for code in all_codes_short:
                code.append(color)  # add to each code the color it ends with
                all_codes.append(code)  # add the code to all the codes
        return all_codes

    def code_to_index(self, code):
        index = 0
        for i in range(len(code)):
            color = code[i]
            color_num = ord(color) - 65
            index += color_num * self.colors_num ** i  # counting in base n where n in the number of different colors
        return index

    def get_state_vector(self, guesses, clues):
        """
        :return: a vector that says for each action if it is consistent with
        the past guess and clues. 1 if consistant 0 else.
        the vector returned is a list of length code_options
        """
        state_vector = []
        for code in self.code_options:
            if self.check_consistency(code, guesses, clues):
                state_vector.append(1)
            else:
                state_vector.append(0)
        return state_vector

    def check_consistency(self, code, guesses, clues):
        """
        :param guess: player next guess
        :return: check if it is valid with the game
        """
        is_valid = True
        for i in range(len(guesses)):
            is_valid = self.match_clue(code, guesses[i], clues[i])
            if not is_valid:
                return False
        return is_valid

    def match_clue(self, code, guess, clue):
        """
        :param guess: player next guess
        :param move: a past move of the player
        :param clue: the clue the player got for this move
        :return: True if the guess is consist with the move and the clue
        False otherwise
        """
        temp_clues = []
        temp = guess.copy()  # need to copy the code array, so we can change it

        holdguess = list(code)

        # loop to check for black pegs
        for i in range(self.pegs_num):
            if holdguess[i] == temp[i]:
                temp_clues.append("X")  # black peg-correct letter and position
                temp[i] = "m"  # remove correct location from code checking array
                holdguess[i] = "q"  # take out of guess so not counted as white peg

        # loop to check for white pegs
        for j in range(self.pegs_num):
            if temp[j] in holdguess:
                temp_clues.append("O")  # white peg-correct letter, wrong position
                holdguess.remove(temp[j])  # remove from check so not counted twice

        temp_clues.sort(reverse=True)  # sort so X's are always first so player can't tell which are correct
        temp_clue = ''.join(temp_clues)  # convert list to a string with no spaces and return

        if temp_clue == clue:
            return True
        else:
            return False

if algorithem == "q-learning":
    game = palti(color_number, num_pegs)
    agent = qlearningAgents.MMDeepQLearning(game)
    agent.alpha = 0.1
    agent.epsilon = 0
    agent.discount = 0.8
    agent.train = False
    path = "ql_6_4_net"
    agent.load_agent(path)


def userAction():
    canvas.unbind('<space>')
    if is_human:
        canvas.bind('<Left>', lambda _: selectPos(-1))
        canvas.bind('<Right>', lambda _: selectPos(1))
        canvas.bind('<Up>', lambda _: switchColor(1))
        canvas.bind('<Down>', lambda _: switchColor(-1))
    canvas.bind('<Return>', lambda _: switchrow())

def userInAction():
    canvas.unbind("<Left>")
    canvas.unbind("<Right>")
    canvas.unbind("<Up>")
    canvas.unbind("<Down>")
    canvas.unbind("<Return>")

def createCode():
    selection = [x for x in range(len(basecolors))]
    code = []
    for i in range(codeLength):
        codeIndex = random.randint(0,len(selection)-1)
        code.append(selection[codeIndex])
        selection.pop(codeIndex)
    return code

codedColor = createCode()

def initRow():
    global selectColors,tops, bots
    selectColors = [x for x in range(len(basecolors))]
    tops = 0
    bots = 0

def initGame():
    global row, cpos, colorpicks, codedColor
    canvas.itemconfig(board[row][cpos],width=0)
    for i in range(noOfGuesses):
        for j in range(codeLength):
            canvas.itemconfig(board[i][j], fill='#8888dd')
            if i < noOfGuesses - 1:
                canvas.itemconfig(response[i][j], fill='#8888dd')
    colorpicks = [[-1 for i in range(codeLength)] for j in range(noOfGuesses)]
    row = 0
    cpos = 0
    canvas.itemconfig(board[row][cpos],width=1)
    userAction()
    codedColor = createCode()
    #print(codedColor)
    initRow()

board = []
response = []
for i in range(noOfGuesses):
    newRow = []
    newResponse = []
    for j in range(codeLength):
        x = colorpadding*j+5
        y = height2turns*noOfGuesses - colorpadding*i - colorSize - 5
        newRow.append(canvas.create_oval(x,y,x+colorSize,y+colorSize,fill='#8888dd',outline='black',width=0))
        if i < noOfGuesses-1:
            x = colorpadding/2*j+colorpadding*codeLength
            y += colorSize/8
            newResponse.append(canvas.create_oval(x+ colorSize/4, y+ colorSize/4, x + colorSize/2, y + colorSize/2, fill='#8888dd', outline='black', width=0))
    board.append(newRow)
    if i < noOfGuesses - 1:
        response.append(newResponse)
initGame()
canvas.itemconfig(board[row][cpos],width=1)

def select(colorPosition):
    canvas.itemconfig(colorPosition, width=5)

def deselect(colorPosition):
    canvas.itemconfig(colorPosition, width=0)

def setcolor(colorPosition,color):
    canvas.itemconfig(colorPosition, fill=color)

def selectPos(increment):
    global cpos
    canvas.itemconfig(board[row][cpos],width=0)
    cpos += increment
    if cpos < 0: cpos = codeLength-1
    if cpos >= codeLength: cpos = 0
    canvas.itemconfig(board[row][cpos],width=1)

def switchColor(increment):
    colorpicks[row][cpos] += increment
    if colorpicks[row][cpos] > len(basecolors)-1: colorpicks[row][cpos] = 0
    if colorpicks[row][cpos] < 0: colorpicks[row][cpos] = len(basecolors)-1
    canvas.itemconfig(board[row][cpos], fill=basecolors[colorpicks[row][cpos]])

def make_ai_move(curr_row, curr_colorpicks, curr_clues):
    colors = [chr(65 + _) for _ in range(color_number)]
    pegs_num = codeLength
    guesses = []
    for element in curr_colorpicks:
        temp = []
        for i in range(pegs_num):
            if element[i] != -1:
                temp.append(chr(ord("A")+element[i]))
        guesses.append(temp)
    clues = []
    for element in curr_clues:
        temp = []
        for i in range(pegs_num):
            if element[i] == 1:
                temp.append('X')
            if element[i] == 0:
                temp.append('O')
        clues.append("".join(temp))

    new_guesses = []
    for e in guesses:
        if e != []:
            new_guesses.append(e)
    new_clues = []
    for e in clues:
        if e != []:
            new_clues.append(e)

    if algorithem == "genetic":
        ga_wrapper = genetic_algorithem.GAWRAPPER(colors, pegs_num, guesses, clues)
        return str2num_color(ga_wrapper.run_ga())

    if algorithem == "hill":
        new_guesses = []
        for e in guesses:
            if e != []:
                new_guesses.append(e)
        new_clues = []
        for e in clues:
            if e != []:
                new_clues.append(e)
        return str2num_color(hill_climbing.choose_guess(colors, pegs_num, new_guesses, new_clues))

    if algorithem == "csp":
        new_guesses = []
        for e in guesses:
            if e != []:
                new_guesses.append(e)
        new_clues = clues[:len(new_guesses)]
        return str2num_color(computer_player.make_guess(new_guesses, new_clues))

    if algorithem == "q-learning":
        new_guesses = []
        for e in guesses:
            if e != []:
                new_guesses.append(e)
        new_clues = clues[:len(new_guesses)]
        state_vector = game.get_state_vector(new_guesses, new_clues)
        return str2num_color(agent.getAction(state_vector))

def str2num_color(temp):
    temp = list(temp)
    res = []
    for e in temp:
        res.append(ord(e) - ord("A"))
    return res


def checkguess(guess, code):
    temp_clues = []
    temp = code.copy()
    tops = 0
    bots = 0

    holdguess = guess

    # loop to check for black pegs
    for i in range(num_pegs):
        if holdguess[i] == code[i]:
            temp_clues.append("X")  # black peg-correct letter and position
            tops += 1
            temp[i] = "m"  # remove correct location from code checking array
            holdguess[i] = "q"  # take out of guess so not counted as white peg

    # loop to check for white pegs
    for j in range(num_pegs):
        if temp[j] in holdguess:
            temp_clues.append("O")  # white peg-correct letter, wrong position
            bots += 1
            holdguess.remove(temp[j])  # remove from check so not counted twice

    temp_clues.sort(reverse=True)  # sort so X's are always first so player can't tell which are correct
    clue = ''.join(temp_clues)  # convert list to a string with no spaces and return

    return tops, bots

def switchrow():
    global row, colorpicks, clues

    if not is_human:
        ai_move = make_ai_move(row, colorpicks, clues)
        for i in range(len(ai_move)):
            colorpicks[row][i] = ai_move[i]
        for i in range(codeLength):
            canvas.itemconfig(board[row][i], fill=basecolors[colorpicks[row][i]])

    for i in range(codeLength):
        if colorpicks[row][i] == -1:
            #print("Colors not set {},{}:".format(row,i))
            print("Colors not set")
            return False

    tops, bots = checkguess(colorpicks[row][:], codedColor)
    if tops < codeLength and row < noOfGuesses-2:
        for i in range(tops):
            clues[row][i] = 1
            canvas.itemconfig(response[row][i], fill="black")
        for i in range(bots):
            clues[row][i+tops] = 0
            canvas.itemconfig(response[row][i+tops], fill="white")
        canvas.itemconfig(board[row][cpos],width=0)
        row += 1
        canvas.itemconfig(board[row][cpos],width=1)
        initRow()
        return False
    else:
        print("Row{} tops{} and bots{}".format(row,tops,bots))
        output = True
        if row == noOfGuesses-2:
            output = False
        for i in range(tops):
            canvas.itemconfig(response[row][i], fill="black")
        for i in range(bots):
            canvas.itemconfig(response[row][i+tops], fill="white")
        for i in range(codeLength):
            canvas.itemconfig(board[noOfGuesses-1][i], fill=basecolors[codedColor[i]])
        userInAction()
        canvas.bind("<space>", lambda _: initGame())
        return output

frame.pack()
canvas.pack()

root.title("Mastermind")

canvas.focus_set()

userAction()

canvas.bind("<space>", lambda _: initGame())

root.mainloop()
