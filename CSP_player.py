import random
import copy
import numpy as np
import operator

class CSPPlayer:
    def __init__(self, colors, num_pegs):
        self.colors = colors
        self.num_color = len(colors)
        self.num_pegs = num_pegs
        self.knowledge_dict = {}
        self.init_knowledge()
        self.best_colors = np.zeros((self.num_pegs,self.num_color))
        self.ORD_A = ord('A')
        self.playerguesses = []
        self.clues = []


    def init_knowledge(self):
        """
        init knowledge cidt
        :return: dict. keys are pegs and there values are the possible colors
        """
        for i in range(self.num_pegs):
            self.knowledge_dict[i] = self.colors.copy()

    def match_clue(self, guess,move,clue):
        """

        :param guess: player next guess
        :param move: a past move of the player
        :param clue: the clue the player got for this move
        :return: True if the guess is consist with the move and the clue
        False otherwise
        """
        temp_clues = []
        temp = move.copy()  # need to copy the code array, so we can change it

        holdguess = list(guess)

        # loop to check for black pegs
        for i in range(self.num_pegs):
            if holdguess[i] == temp[i]:
                temp_clues.append("X")  # black peg-correct letter and position
                temp[i] = "M"  # remove correct location from code checking array
                holdguess[i] = "Q"  # take out of guess so not counted as white peg

        # loop to check for white pegs
        for j in range(self.num_pegs):
            if temp[j] in holdguess:
                temp_clues.append("O")  # white peg-correct letter, wrong position
                holdguess.remove(temp[j])  # remove from check so not counted twice

        temp_clues.sort(reverse=True)  # sort so X's are always first so player can't tell which are correct
        temp_clue = ''.join(temp_clues)  # convert list to a string with no spaces and return

        if temp_clue == clue:
            return True
        else:
            return False


    def check_consistency(self,guess):
        """
        :param guess: player next guess
        :return: check if it is valid with the game
        """
        is_valid = True
        for i in range(len(self.playerguesses)):
            is_valid = self.match_clue(guess,self.playerguesses[i], self.clues[i])
            if not is_valid:
                return False
        return is_valid

    def update_knowledge(self):
        if self.clues != []:
            if self.clues[-1] == '':
                colors2delete = set(self.playerguesses[-1])
                for color in colors2delete:
                    for peg in self.knowledge_dict:
                        if color in self.knowledge_dict[peg]:
                            self.knowledge_dict[peg].remove(color)
            elif len(self.clues[-1]) == self.num_pegs:
                colors = set(self.playerguesses[-1])
                for peg in self.knowledge_dict:
                    self.knowledge_dict[peg] = list(colors)
            elif not "X" in self.clues[-1]:
                for peg in self.knowledge_dict:
                    self.knowledge_dict[peg].remove(self.playerguesses[-1][peg])
            num_X = self.clues[-1].count('X')
            for peg in range(self.num_pegs):
                self.best_colors[peg,ord(self.playerguesses[-1][peg]) - self.ORD_A] += num_X




    def make_guess(self, playerguesses, clues):
        self.playerguesses = playerguesses
        self.clues = clues
        return self.backtracking()

    def backtracking(self):
        self.update_knowledge()
        guess = []
        for i in range(self.num_pegs):
            guess.append('')
        order_domain_variable = self.knowledge_dict.copy()
        return self.recursive_backtracking(guess,order_domain_variable)

    def select_unassigned_peg(self,guess,order_domain_variable):
        order_pegs = sorted(order_domain_variable, key=lambda k: len(order_domain_variable[k]))
        for peg in order_pegs:
            if guess[peg] == '':
                return peg

    def select_random_color(self,order_domain_variable,peg):
        colors = order_domain_variable[peg]
        if not colors :
            return False
        return random.choice(colors)


    def select_mk_color(self,order_domain_variable,peg):
        colors = order_domain_variable[peg]
        if not colors :
            return False
        color_dict = {}
        for color in colors:
            color_dict[color] = self.best_colors[peg,ord(color) - self.ORD_A]
        max_score = max(color_dict.items(), key=lambda x: x[1])
        list_of_colors = list()
        # Iterate over all the items in dictionary to find keys with max value
        for color, score in color_dict.items():
            if score == max_score[1]:
                list_of_colors.append(color)
        best_color = random.choice(list_of_colors)
        return best_color





    def is_guess_complete(self,guess):
        is_full = True
        if '' in guess:
            is_full = False
        is_new = True
        if guess in self.playerguesses:
            is_new = False
        is_valid = self.check_consistency(guess)
        return is_full*is_valid*is_new

    def recursive_backtracking(self, guess,order_domain_variable):
        if self.is_guess_complete(guess):
            return ''.join(guess)
        if not '' in guess:
            return False
        peg = self.select_unassigned_peg(guess, order_domain_variable)
        while(True):
            color = self.select_mk_color(order_domain_variable,peg)
            if not color:
                break # if there is no color then break the loop
            guess[peg] = color
            odv_copy = copy.deepcopy(order_domain_variable)
            result = self.recursive_backtracking(guess,odv_copy)
            if result != False:
                return result
            guess[peg] = ''
            order_domain_variable[peg].remove(color)
        return False
